using MediatR;

namespace Shop.Domain.Requests.Categories;

public class DeleteCategoryRequest : IRequest
{
    public int Id { get; set; }
}