﻿namespace Shop.Domain.Enums;

public enum OrderStatus
{ 
    Confirmed,
    NotConfirmed,
    Canceled,
    Finished
}