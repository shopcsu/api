﻿using Shop.Domain.Enums;

namespace Shop.Domain.Dtos;

public class OrderDto
{
    public Guid Id { get; set; }
    public DateTime Created { get; set; }
    public int TotalPrice { get; set; }
    public ICollection<ItemDto> Items { get; set; } = new List<ItemDto>();
    public OrderStatus Status { get; set; }
}