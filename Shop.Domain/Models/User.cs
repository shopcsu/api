using Shop.Domain.Enums;

namespace Shop.Domain.Models;

public class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Hash { get; set; }
    public Role Role { get; set; }
    public ICollection<Token> Tokens { get; set; }
}