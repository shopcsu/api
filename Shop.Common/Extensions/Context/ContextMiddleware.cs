using Microsoft.AspNetCore.Http;
using Shop.Domain.Enums;

namespace Shop.Common.Extensions.Context;

public class ContextMiddleware : IMiddleware
{
    private readonly RequestContext _context;

    public ContextMiddleware(RequestContext context) =>
        _context = context;

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (context.User.Identity is { IsAuthenticated: true })
        {
            _context.IsAuthenticated = true;
            _context.UserId = int.Parse(context.Request.Headers["Id"]);
            _context.Role = Enum.Parse<Role>(context.Request.Headers["Role"]);
        }

        await next(context);
    }
}