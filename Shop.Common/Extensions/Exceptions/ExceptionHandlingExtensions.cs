﻿using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Shop.Common.Extensions.Exceptions;

public static class ExceptionHandlingExtensions
{
    public static void UseApiExceptionHandler(this IApplicationBuilder app)
    {
        var logger = app.ApplicationServices.GetRequiredService<ILogger<ExceptionHandler>>();
        var handler = new ExceptionHandler();

        app.UseExceptionHandler(pipeline =>
        {
            pipeline.Run(context =>
            {
                context.Response.ContentType = "application/json";

                var feature = context.Features.Get<IExceptionHandlerFeature>();
                if (feature == null) return Task.CompletedTask;

                try
                {
                    var (code, response) = handler.Handle(feature.Error, context);

                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = (int)code;
                    return context.Response.WriteAsJsonAsync(response);
                }
                catch (Exception e)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    return context.Response.WriteAsJsonAsync(new ExceptionDto
                    {
                        Type = e.GetType().Name,
                        Message = !string.IsNullOrEmpty(e.Message)
                            ? e.Message
                            : "Что-то пошло не так",
                    });
                }
                finally
                {
                    if (handler.Report(Enum.Parse<HttpStatusCode>(context.Response.StatusCode.ToString()),
                            feature.Error,
                            context.Request))
                    {
                        logger.LogError(feature.Error,
                            "An unhandled exception has occurred while executing the request");
                    }
                }
            });
        });
    }
}