using System.Reflection;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Shop.Common.Extensions.Amqp;
using Shop.Orders.Consumer;
using Shop.Orders.Consumer.Database;
using Shop.Orders.Consumer.Options;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<Worker>();
        services.AddAmqp();
        services.AddMediatR(Assembly.GetExecutingAssembly());
        services.AddOptions<DatabaseOptions>().BindConfiguration("Database");
        services.AddDbContext<OrdersContext>((ctx, builder) =>
        {
            var options = ctx.GetRequiredService<IOptions<DatabaseOptions>>()
                .Value;

            builder.UseNpgsql(options.Connection);

            if (options.EnableLogs)
            {
                builder.EnableDetailedErrors()
                    .EnableSensitiveDataLogging();
            }
        });
    })
    .Build();

await host.RunAsync();