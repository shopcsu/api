using MongoDB.Driver;
using Shop.Catalog.Dtos;

namespace Shop.Catalog.Database;

public class MongoReader
{
    private readonly IMongoDatabase _database;

    public MongoReader(IMongoDatabase database)
    {
        _database = database;
    }

    public async Task<CategoryDto> GetCategory(int id, CancellationToken ct = default)
    {
        var col = _database.GetCollection<CategoryDto>("categories");
        return await col.Find(x => x.Id == id).FirstOrDefaultAsync(ct);
    }

    public async Task Reinit()
    {
        var col = _database.GetCollection<CategoryDto>("categories");
        await col.DeleteManyAsync(_ => true);
        await col.InsertOneAsync(new CategoryDto
        {
            Id = -1,
            Title = string.Empty
        });
    }
}